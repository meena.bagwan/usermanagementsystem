# User management Single Page App
## Prerequisite
- Install node js
- Clone the repository

## Setup & Run the project
- Go to the source code directory
- Install dependencies
    - ```npm install```
- Start the server
    - ```npm start```

## Functionality
- Add User
- Search User
- Display user list
- Delete user
- Search functionality i have provided on onclick button.
    - Add Name or email on search box then click on search button.
    - If again want to show users then blank the serach textbox and click on search button.

