import { useState, useEffect } from "react";
import "./App.css";

const App = () => {
  const [users, setUsers] = useState([]);
  const [filterUsers, setfilterUsers] = useState([]);
  const [search, setSearch] = useState([]);
  const [state, setState] = useState({
    id: "",
    name: "",
    email: "",
    password: "",
    error: "",
  });

  const { id, name, email, password, error } =
    state;
  const [passwordVisible, setPasswordVisibility] = useState(false);

  useEffect(() => {
    fetchAllUsers();
  }, []);

  //Password show or hide
  const togglePasswordVisibility = () =>
    setPasswordVisibility((visible) => !visible);

  //Display error message
  const setError = (error = "") => setState((state) => ({ ...state, error }));

  //Handle input state 
  const handleInputChange = (e) => {
    const {
      target: { name, value },
    } = e;
    setState((state) => ({ ...state, [name]: value, error: "" }));
  };

  //Handle search state
  const handleSearchChange = (e) => setSearch(e.target.value)

  //On serach function [search by name and email]
  const onSearchClick = () => {
    let filterUsers = users.filter(({ name, email }) => {
      return name?.toLowerCase()
        .includes(search?.toLowerCase()) || email?.toLowerCase()
          .includes(search?.toLowerCase());
    })
    setfilterUsers(filterUsers)
  }

  //Delete the use by id
  const deleteUser = (id) => {
    fetch(`http://apitest.pluto-men.com/api/user/${id}`, {
      method: "DELETE"
    })
      .then((res) => res.json())
      .then(() => {
        fetchAllUsers();
        alert("User Deleted successfully");
      })
      .catch((err) => alert(err.message));

  }

  //Submit button call function
  const handleSubmit = () => {
    if (!name.trim()) {
      setError("Name is required");
      return;
    }
    if (!email.trim() || !email.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
      setError("Email is required / Invalid email");
      return;
    }
    if (!password.trim()) {
      setError("Password is required");
      return;
    }
    const data = {
      name,
      email,
      password,
    };
    fetch("http://apitest.pluto-men.com/api/user", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then(() => {
        fetchAllUsers();
        alert("User added successfully");
        setState({
          name: "",
          password: "",
          email: "",
          error: "",
        });
      })
      .catch((err) => alert(err.message));
  };

  //Fetch all user
  const fetchAllUsers = () => {
    fetch("http://apitest.pluto-men.com/api/user")
      .then((res) => res.json())
      .then((response) => {
        setUsers(response);
        setfilterUsers(response);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="container">
      <h1>User Management App</h1>
      <div className="form">
        <div>
          <div className="label">Name</div>
          <input
            required
            name="name"
            type="text"
            placeholder="Name"
            value={name}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <div className="label">Email</div>
          <input
            required
            name="email"
            type="email"
            placeholder="Email"
            value={email}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <div className="label">Password</div>
          <div className="password-input">
            <input
              required
              name="password"
              type={passwordVisible ? "text" : "password"}
              value={password}
              onChange={handleInputChange}
              onKeyDown={(e) => e.key === "Enter" && handleSubmit()}
            />
            <span onClick={togglePasswordVisibility}>
              {passwordVisible ? "Hide" : "Show"}
            </span>
          </div>
        </div>
        {error && <p className="error">{error}</p>}
        <button onClick={handleSubmit}>Submit</button>
      </div><br />
      <div className="searchDiv">
        <input
          type="text"
          value={search}
          placeholder="Search by name or email..."
          onChange={handleSearchChange} />&nbsp;&nbsp;
        <button onClick={onSearchClick} className="search">Search</button>&nbsp;&nbsp;
      </div>
      <br /><b>Users List</b><br />
      {
        filterUsers.map((user) => (
          <div key={user.id} className="profile-card">
            <div style={{ display: 'flex' }}>
              <div >
                <b>ID : {user.id}</b>
              </div >
              <div style={{ paddingLeft: '20px' }}> Name : {user.name}</div>
              <div style={{ paddingLeft: '50px' }}><button className="delete" onClick={() => deleteUser(user.id)}>delete</button></div>
            </div>
          </div>
        ))
      }
    </div>
  );
}

export default App;
